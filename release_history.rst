FREEWAT Release History
=======================
Release v.1.1.0
---------------

Master
^^^^^^
* Improvement of Particle Tracking GUI
* Small bug fix in Plot Model Fit

Release v.1.0.2
---------------

Master
^^^^^^
* Including the possibility to create non-regular grids
* A new tool to create a Telescopi refinement of an existing regular grid 
* Small bug fix in Copy To Vector

Water management and crop modelling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Small bug fix in Crop Growth Module

Calibration
^^^^^^^
* Small bug fix: lower and upper constraint values were reversed!! 

OAT
^^^
* Fixed problem when uploading time series with duplicates

Release v.1.0
-------------

Master
^^^^^^
* Improvement of Copy to Vector: now the Copy function works only on selected features in input layer (if any)
* Run Model speed-up: info on several MDOs is now gathered directly from DB, which reduces a lot the time for creating input files
* Several Progress Bars included during Run Model process (giving info to User about the model data pre-processing)
* Improvement of WEL Package: it is possible also to automatically select WELL cells in your grid, using a linear shape file in input
* Improvement of CHD Package: it is possible to create CHE MDO starting from a linear shape file, and getting the Head (in each cell) as interpolated values, giving as input In and Out head values (as for RIV, GHB, etc.) 
* Improvement of GHB Package: it is possible to input In and Out head values, to get the head values (in each cell) as interpolated values 
* Installation procedure has been modified, to be compliant with QGIS Plugin Repository  
* Automated testing procedures included for several tools, for Developers' convenience 

Water management and crop modelling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Crop Growth Module is now a separate tool, independent upon Run Model GUI, accessible directly from FREEWAT > Water Management sub-menu

Release v.0.5
-------------

Master
^^^^^^
* Add USE and GROUP field to MDO for WEL Package
* Speed up of Copy to Vector between Spatialite layers (e.g. between model layers)
* Possibility to apply Copy to Vector tool only on selected features of source layer 
* Pop-up messages for missing exe files in ProgramLocations table
* Fix bug in SFR2 object builder, when flopy >= 3.2.6
* MODPATH (Particle Tracking) Fixing, Update and Improvement: now you can create several MODPATH simulations and starting particles from a selected area where RCH or WEL are applied (backward and forward) 
* Update on ZoneBudget output: now a CSV file with budget for each zone is saved in your working directory
* Speed up of Run Model (1st stage: to retrieve grid size)
* Bug fixing in SSM WEL: only with selected SPs it did not work. 
* Small bug fixing in running UZF with flopy >= 3.2.6
 

Calibration
^^^^^^^
* Solving small bugs in calibration: (i) when no multi-layer observation is present; (ii) reducing number of figures in floating point format when negative numbers are present: this prevents bad writing of HOB input file



Release v.0.4.1
-------------

Master
^^^^^^
* Now FREEWAT is compliant with latest flopy version 3.2.6

Calibration
^^^^^^^
* Calibration plots are now working under Linux, Win XP, 7, 10


Release v.0.4
-------------

Master
^^^^^^
* Update of csv_templates folder
* Post-processing) Including a tool for cross section visualization of model result (flow and transport)
* Installation) Fix and improve several parts in pip and numpy installation (Windows and Linux ), including the one fro proxy port
* Run Model) Enhancement on window showing the final message after simulations (MOFLOW, MODFLOW-OWHM, MT3D, SEAWAT)
* Post-processing) Saving Vol. Budget as CSV: now the CSV contains Volumes and Flow rates for all the Stress Periods
* Enhanced error handling on LAK interface
* Fix import LAK package
* Modified import from csv to define lake stress periods (LAK)
* Add possibility to upload stress period data from csv (LAK)
* Add Italian translation for Create LAK ui

AkvaGIS
^^^^^^^
* Fix time plot icons in akvaGIS

Transport
^^^^^^^^^
* Including Package UZT (Unsaturated Zone Transport) of new code MT3D-USGS
* Fix bugs in Transport, in case of multi-component models

OAT
^^^
* Fix problem when loading data from hob file (missing measures)
* Add italian translation to OAT interface
* Add possibility to select all available sensors when loading from istSOS
* Add possibility to upload multiple sensors from IstSOS


Release v.0.3
-------------

Master
^^^^^^
* Improvement of the installation procedure, needed if some Python libraries are missing.  
* Improvements on RIV, GHB and DRN package: 
	1. it is possible to create a unique MDO for multi-line river (or drain or ghb boundary) directly from a multi-line GIS layer. 
	2. speed up of MDOs creation
* Help files included in (almost) GUIs for MODFLOW Boundary conditions

Calibration
^^^^^^^^^^^
* Possibility to apply zones option to LPF and RCH/EVT parameters table 
* Model fit plot and main statistics included under Post-processing menu
 

Release v.0.2
-------------

Master
^^^^^^
* New method to create MDO (Model Data Object) for Model Layer (corresponding to the MODFLOW layer for rendering the vertical discretization), and package usually defined on the whole grid (RCH, EVT, UZF, FARM IDs, SSM for RCH and EVT).
This new procedure allows a huge speed up of creation (for instance, on a 6GB RAB with Windows 10, a model layer of 201824 cells is now created in 2.5 minutes, instead of 1.5 hours in the former version!)
* MODPATH is now included, under Post-processing menu. It applies the particle tracking method to results coming from a MODFLOW simulation.
At this stage, this tool is tested only with the link to WEL package, in backward mode, for defining the capture zones related to any well present in the model. A specific tutorial for the application of this tool is cooming soon.

 
Release v.0.1
-------------

Master
^^^^^^
* New tool Update Working Directory added to Model Setup menu: to facilitate the User while importing external models
* Progress bars included in all main packages: to facilitate the User on understanding the process running
* New Program Location UI: to facilitate the User in changing/updating the path of executable codes
* New LPF table formatting: to help the User when changing model layers properties
* Bugs fixing to allow the platform being fully Linux compatible
* A new installation file included in the sub-folder installation, within the main folder of plugin: to help the User in installing missing libraries needed by FREEWAT
* Including a sub-folder with CSV templates, within the main folder of plugin
* Deselect features after creating the MDOs: to avoid possible User’s error in creating the model inputs

Akva
^^^^
* The AkvaGIS tools have been adapted to be compatible with the LTR of QGIS 2.14 Essen
* Different capabilities for improving the introduction of new input data such us the dropdown lists have been improved and updated
* The selection of hydrochemical parameters for being queried by the different instruments of the hydrochemical analysis tools such as Chemical Parameter Plot has been improved. In the new version, the user selects just the name of the parameter and the different analyses independently of the unit of measurements are shown (together with its units of measurements)
* Additional time-analysis capabilities have been included for query and represent the time evolution of the hydrogeological parameters
* The possibility of exporting the results obtained with the Exporting Tools of Hydrochemical Analysis Tools have been improved and extended to other formats (e.g. cvs)

Transport
^^^^^^^^^
* Including the possibility of using FloPy version 3.2.4: this option is needed to avoid modeling only scenario with uniform transport parameters (due to a bug in the former version of FloPy)

Water management and crop modelling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Changing the way of setting crop coefficients and reference evapotranspiration values as the former version required input data that are too difficult to be retrieved by the User
* Including Plot budgets for Water Units, under Post-processing menu in order to plot water budgets for a single water demand unit
* Improvements according to trainees’ requests: external deliveries, groundwater and surface-water allotments are now time-dependent

Calibration
^^^^^^^^^^^
* Including definition and management of  2D time-variant parameters (packages RCH, EVT)
* Including the possibility to run UCODE also for RCH and EVT parameters
* Including Plot Calibration under Post-processing: to plot results of sensitivity and calibration

Lake package
^^^^^^^^^^^^
* Enhanced stress period parameters definition
* Added the possibility to edit lake parameters directly into the UI
* Including the possibility to reload lak_layer_group if it is present into the DB, but not in the QGIS layer tree

OAT
^^^
* Added missing fields to sensor
* Fixed problems with hobfile input source
* Enhanced sensor cloning
* Enhanced time series plot
* Fix problem with matplotlib library versioning
* Created dropdown with predefined date format when importing from CSV file
* Fixed problem with Process UI behavior when trying to save multi-sensors (like HydroSeparation result)
* Changed name of the layer from freewat sensors to oat_sensors
* Added oat_sensors layer to QGIS when creating the first sensor
* Added save sensor to csv
* CSV preview now start from first value
* Added the possibility to create HOB layer using OAT sensors

Release Beta v.1.5
------------------

Master
^^^^^^
* Including tool for merging Spatialite layers: to merge MDOs and tables in Spatialite DB
* Including Model Budget Viewer under Post-processing: to show histogram of the model water budget


Akva
^^^^
* The AkvaGIS database has been improved: the code list of different hydrogeological and hydrochemical terms has been updated and some errors such us duplicates or missing index have been corrected
* Some minor errors regarding the Ionic Balance Calculations have been corrected
* Some bugs fixing

Trasnport
^^^^^^^^^
* Including Unsaturated Solute Balance module (USB): to simulate contaminant leaching through the unsaturated zone

Water management and crop modelling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* No relevant changes, a part bugs fixing

Calibration
^^^^^^^^^^^
* Including Flow Observation packages (for RIV,DRN,GHB,CHD)
* Including definition and management of 3D parameters (package LPF)
* Including UCODE to run sensitivity and calibration (related to LPF parameters)

Lake package
^^^^^^^^^^^^
Created LAK package module, and included within MODFLOW Boundary Conditions menu

OAT
^^^
* Added additional input source (hoblayer, gagefile, listfile)
* Added SensorCompare UI
* Enhanced sensor management (cloning)
* Added QGIS layer
* Fixed problems on some libraries dependency

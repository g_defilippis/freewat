# import moduli
from datetime import datetime, timedelta, date
from freewat_utils import getVectorLayerNames, getModelsInfoLists, getModelInfoByName
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from pyspatialite import dbapi2 as sqlite3


createCrop(dbName, ):
    '''
    dbName = nome DB da cui prendere info
    '''


# dove layerNameList e la combobox con la lista dei layer caricati nella legenda
    layerNameList = getVectorLayerNames()

    # in base a layer nella TOC cerca la modeltable e restituisce nome e working directory
    (modelNameList, pathList) =  getModelsInfoLists(layerNameList)

    # match the exact model trough the list of all models
    db = [os.path.basename(dbName).replace('.sqlite', '')]
    for i in db:
        if i in modelNameList:
            modelName = 'modeltable_' + str(i)
            timetableName = 'timetable_' + str(i)


    # Get model info from modeltable and timetable
    model = getVectorLayerByName(modelName)
    timetable = getVectorLayerByName(timetableName)

    # recupero info su data e tempo iniziali da modeltable
    for ft in model.getFeatures():
        date0str = ft['initial_date']

    # trasformo la data iniziale in formato data
    date0 = datetime.strptime(date0str, "%Y-%m-%d")
    date0 = date0.date()

    # recupero info su numero SP, lunghezza SP, numero TS per ogni SP
    # queste informazioni mi servono per trovare i TS corrispondenti a una data inserita manualmente

    sp = []
    time_step = []
    length = []

    for i in timetable.getFeatures():
        sp.append(int(i['sp']))
        ts.append(int(i['ts']))
        ll.append(int(i['length']))


    # pensare a combobox per inserire la data finale (sarà uguale a date_fin)
    date_fin_str = .....
    # sarà letta come stringa, quindi trasformazione da stringa a formato data
    date_fin_0 = datetime.strptime(date_fin_str, "%Y-%m-%d")
    date_fin_0 = date0.date()

    # recupero lunghezza TS come semplice differenza fra data_fin e data_ini

    diff = date_fin_0 - date0
    # converto da date a giorni (intero)
    diff = diff.days()






    # funzione che per ora non serve (ma può tornare utile)
    # funzione per recuperare il TS da data in input rispetto a data iniziale
    def get_TS(date_ini, date_fin, length, ts):
        '''
        date_ini = data iniziale (initial_date, presa dalla modeltable)
        date_fin = data finale (inserita dall'utente)
        length = lunghezza SP (presa da modeltable)
        ts = numero TS (preso da timetable)
        '''
        # trasformo data_ini da lista a singola stringa
        date_ini = ', '.join(map(str, date_ini))
        date_ini = datetime.strptime(date_ini, '%Y-%m-%d')
        length = timedelta(days = length)
        date_fin = datetime.strptime(date_fin, '%Y-%m-%d')
        delta1 = date_fin - date_ini
        quad = int(((float(delta1.days) / length.days) * ts))
        # restituisce TS in cui ricade data_fin (+1 perche indice è pythonico)
        return quad + 1

    # creo lista tss con il numero corrispondente di TS per ogni TS rispetto alla data inserita manualmente
    tss = []
    for i, j in enumerate(ll):
        tss.append(get_TS(date_ini, date_final, length[i], ts[i]))

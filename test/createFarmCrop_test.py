# coding = utf-8
import sys
import os
import sqlite3
from tempfile import gettempdir
from qgis.core import *
from PyQt4.QtGui import QDialogButtonBox
from .utils import create_test_model
from ..dialogs.createFarmCrop_dialog import createFarmCropSoilDialog

app = QgsApplication(sys.argv, True)
QgsApplication.initQgis()

try:
    dlg = createFarmCropSoilDialog(iface=None)
    assert(False) # should trigger an exception because model is missiing
except RuntimeError as e:
    assert(str(e).find('create a MODEL before') != -1)

dbfile = os.path.join(gettempdir(), 'mymodel.sqlite')
create_test_model(dbfile, app)

dlg = createFarmCropSoilDialog(iface=None)
dlg.textEdit.setText('crop')

if len(sys.argv) > 1:
    dlg.show()
    app.exec_()
else:
    dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)
    app.processEvents()

con = sqlite3.connect(dbfile)
cur = con.cursor()
cur.execute("select count(1) from crop_soil_crop")
assert(cur.fetchone()[0]==100)
con.close()


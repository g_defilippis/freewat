#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from PyQt4.Qt import Qt, QAbstractTableModel, QModelIndex, QBrush, QColor
from model.SavedQueryMeasurementsModel import MEASUREMENT_RESULTS
from model.SamplesMeasurementsModel import SamplesMeasurementsModel
from PyQt4 import QtCore

class SamplesMeasurementsModelHydro(SamplesMeasurementsModel):
    def __init__(self, parent=None): 
        super(SamplesMeasurementsModelHydro, self).__init__(parent)
        self.fixedHeaders = ["PointId", "Point", "Coordinate X", "Coordinate Y", "Campaign", "Sample", "Date"]
        self.fixedFields = [MEASUREMENT_RESULTS.PointId,
                            MEASUREMENT_RESULTS.Point,
                            MEASUREMENT_RESULTS.XCoord,
                            MEASUREMENT_RESULTS.YCoord,
                            MEASUREMENT_RESULTS.Campaign,  
                            MEASUREMENT_RESULTS.Observation,
                            MEASUREMENT_RESULTS.MeasurementDate]
        self.uniqueField = MEASUREMENT_RESULTS.MeasurementId
        
        self.COLUMNS = {}
        self.headers = []
        self.allParams = {}
        self.allParamsOrder = []
        
        
        self.columnsFixed = {}
        self.columnsMeasurement = {}
        self.columnsCustom = {}
        
        self.numberOfColumns = 0
        self.numberOfFixedColumns = 0
        self.numberOfMeasurementCols = 0
        self.numberOfCustomCols = 0
        
        # Actual data storage
        self.sampleMeasurements = []
        self.customColumData = []
        
        self.addFixedColumns()    
        

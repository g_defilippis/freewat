#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''''
from PyQt4.Qt import QSqlQuery

from AKFeature import AKFeature
from model.SamplesMeasurementsModel import SamplesMeasurementsModel

class AKFeatureCustomChart (AKFeature):
    '''This feature takes care of looking up additional information in the database for imposed parameters.'''
    def __init__(self, settings, iface, windowTitle = "", parent=None):
        super(AKFeatureCustomChart, self).__init__(iface, parent)
        self.m_settings = settings
        self.imposeParams = True
        self.customParametersOrder = []
        self.allParameterForCharts = {}
        self.sqlSelBaseParam =  ""
        self.typeOfModel = SamplesMeasurementsModel

    def defineSQLQueries(self):
        pass
    
    def initialize(self):
        self.defineSQLQueries()
        self.currentDB = self.m_settings.getCurrentDB()
        self.addParameterDetails()
        self.paramatersOfInterest = dict()
        for parameterId in self.customParametersOrder:
            self.paramatersOfInterest[parameterId] = self.allParameterForCharts[parameterId]
        self.m_measurementsSubFeature.imposeParameters(self.paramatersOfInterest)
        self.m_measurementsSubFeature.initialize()
        
    def onNextForm(self, measurementsModel):
        sampleMeasurementsModel = self.typeOfModel()
        sampleMeasurementsModel.setInitialData(measurementsModel)
        self.m_plotSubFeature.setModel(sampleMeasurementsModel)        
        self.m_plotSubFeature.postProcessModelData(self.paramatersOfInterest, self.customParametersOrder)
        self.m_plotSubFeature.initialize()
        
    def addParameterDetails(self):
        for parameterName in self.allParameterForCharts:
            query = QSqlQuery(self.currentDB)
            query.prepare(self.sqlSelBaseParam)
            query.addBindValue(parameterName)
            query.exec_()
            while query.next():
                self.allParameterForCharts[parameterName]["id"] = query.value(0)
                self.allParameterForCharts[parameterName]["baseParam"] = query.value(1)
                self.allParameterForCharts[parameterName]["name"] = query.value(2)
                self.allParameterForCharts[parameterName]["unit"] = query.value(3)
                self.allParameterForCharts[parameterName]["unitCode"] = query.value(4)
                self.allParameterForCharts[parameterName]["unitId"] = query.value(5)
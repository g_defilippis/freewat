#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''''
from AKFeatureCustomChart import AKFeatureCustomChart
from measurements.AKFeatureMeasurementsHydro import AKFeatureMeasurementsHydro
from model.SamplesMeasurementsModelHydro import SamplesMeasurementsModelHydro

class AKFeatureCustomChartHydro(AKFeatureCustomChart):
    def __init__(self, settings, iface, windowTitle = "", parent=None):
        super(AKFeatureCustomChartHydro, self).__init__(settings, iface, windowTitle, parent)
        self.typeOfModel = SamplesMeasurementsModelHydro
        self.m_measurementsSubFeature = AKFeatureMeasurementsHydro(self.m_settings, self.iface, windowTitle, self.parent())
        self.m_measurementsSubFeature.nextForm.connect(self.onNextForm)
        self.m_measurementsSubFeature.m_parametersAreImposed = False
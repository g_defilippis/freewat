#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from PyQt4.QtGui import QIcon

from AKFeatureCustomChartChem import AKFeatureCustomChartChem
from results.AKFeatureResultsEasyQuim import AKFeatureResultsEasyQuim

class AKFeatureEasyQuim(AKFeatureCustomChartChem):
    '''Can be used to export the geochemical measurements for the EasyQuim program'''
    def __init__(self, settings, iface, parent=None):
        super(AKFeatureEasyQuim, self).__init__(settings, iface, windowTitle = "EasyQuim Export Measurements", parent=parent)
        self.m_icon = QIcon(settings.getIconPath('easyQuim.png'))
        self.m_text = u'EasyQuim Export'
        #Ca, Cl, Mg, Na,SO4,HCO3,K,NH4,NO3,TAC, CE,pH,DUR,TSD, Temperature
        self.customParametersOrder = ["N209MQL","N247MQL","N436MQL","N580MQL","N582MQL","N168MQL",
                                      "N553MQL","N124MQL","N480MQL","N592MQL","N216USC","N540UNK","N326UNK",
                                      "N647UNK","N595GCG"]
        self.m_plotSubFeature = AKFeatureResultsEasyQuim(self.m_settings, self.iface, parent=self.parent())

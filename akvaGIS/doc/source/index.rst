.. AkvaGIS documentation master file, created by
   sphinx-quickstart on Fri Oct 23 14:00:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


AkvaGIS
==================

.. toctree::
   :maxdepth: 4

   akvagis_overview
   akvaGIS   
   modules
   externalModules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`   



